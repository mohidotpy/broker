from flask import Blueprint
devtools_blueprint = Blueprint('devtools', __name__, template_folder='templates')
from . import views