/**
 * Created by BSIM on 6/17/2018.
 */

$(document).ready(function () {


    $('#address').keyup(function () {
        if ($('#coin').val() == 'BTC') {
            if ($(this).val().length >= 32) {
                socket.emit('GET_BALANCE', {coin: $('#coin').val(), wallet: $('#address').val()});
            }
        }
        else if ($('#coin').val() == 'ETH') {
            if ($(this).val().length >= 42) {
                socket.emit('GET_BALANCE', {coin: $('#coin').val(), wallet: $('#address').val()});
            }
        }

    })

    //handle result of etherium and bitcoin wallet check
    socket.on('GET_ETH_BALANCE_RESULT', function (data) {
        $('div.balance').text('your balance :' + data['balance']);
    })


    socket.on('GET_ETH_BALANCE_ERROR', function (data) {
        $('div.balance').text(data['error']);
    })

    // for bitcoin addresses

    socket.on('GET_BTC_BALANCE_RESULT', function (data) {
        $('div.balance').text('your balance :' + data['balance']);
    })


    socket.on('GET_BTC_BALANCE_ERROR', function (data) {
        $('div.balance').text(data['error']);
    })


})
