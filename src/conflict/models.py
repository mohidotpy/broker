from src.transaction.models import Transaction
from datetime import datetime
from src.misc.constants import CONFLICT_STATUS

class Conflict:
    @staticmethod
    def create(tx_id, plaintiff, litigant):
        """
        plaintiff = khahan
        litigant = taraf dava
        """
        tx = Transaction.get(tx_id)
        tx.conflict_status = CONFLICT_STATUS['OPEN']
        pass

    @staticmethod
    def close(tx_id):
        pass

    @staticmethod
    def open(tx_id):
        pass

    @staticmethod
    def get_chats(tx_id):
        pass

    @staticmethod
    def get_status(tx_id):
        """
        returns conflict status
        """
        pass



