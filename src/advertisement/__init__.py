from flask import Blueprint
from flask_login import current_user
PRICE_MODELS = {'MOMENTARY': 'MOMENTARY', 'CUSTOM_DOLLAR': 'CUSTOM_DOLLAR', 'PER_UNIT': 'PER_UNIT'}

advertisement_blueprint = Blueprint('advertisement', __name__, template_folder='template')

from . import views
from . import socket