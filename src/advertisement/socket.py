from src import socketio
from src.coin.models import Coin
from flask_socketio import emit


@socketio.on('CUSTOM_DOLLAR_PRICE')
def CUSTOM_DOLLAR(data):
    dollar = float(data['dollar_price'])
    coin_detail = Coin.get_from_db_by_symbol(data['coin_symbol'])

    toman_result_price = dollar * float(coin_detail.price) * float(data['amount'])


    # round with 2 number after point
    toman_result_price = "%.2f" % toman_result_price
    emit('CUSTOM_DOLLAR_RESULT', {'price': toman_result_price})


@socketio.on('PER_UNIT_PRICE')
def CUSTOM_DOLLAR(data):
    per_unit = float(data['per_unit_price'])
    coin_detail = Coin.get_from_db_by_symbol(data['coin_symbol'])

    toman_result_price = per_unit * float(data['amount'])

    # round with 2 number after point
    toman_result_price = "%.2f" % toman_result_price
    emit('PER_UNIT_RESULT', {'price': toman_result_price})
