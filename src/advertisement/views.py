from . import advertisement_blueprint
from src.user.models import User, Advertisement
from .forms import create_ad_form
from flask import render_template, redirect, url_for, request, flash, abort,jsonify
from flask_login import login_required, current_user
from datetime import datetime
# from src.misc.decorators import ad_owner_or_admin
from src.misc.constants import ROLES
from src.misc.exceptions import EthExplorerException
from flask_babel import gettext
from uuid import uuid4
from src.coin.models import Coin
from mongoengine import errors as MongoEngineErrors
from src.cryptoapi.ethereum_api import EthApi
from src.cryptoapi.bitcoin_api import BitcoinApi


@advertisement_blueprint.route('/')
@advertisement_blueprint.route('/<string:username>/')
@login_required
# @ad_owner_or_admin
def ads_list(username=None):
    if username == current_user.username:
        return redirect(url_for('advertisement.ads_list'))
    if username != None:
        user = User.get(username)
    else:
        user = current_user
    all_list = user.advertisements
    return render_template('/list.html', all_list=all_list, username=username, current_user=current_user)


@advertisement_blueprint.route('/create/', methods=['get', 'post'])
@advertisement_blueprint.route('/create/<string:username>/', methods=['get', 'post'])
@login_required
def create_ad(username=None):
    is_for_another_user = True if current_user.role == ROLES['ADMIN'] \
                                  and username != current_user.username \
                                  and username != None else False
    if is_for_another_user:
        try:
            user = User.get(username)
        except MongoEngineErrors.DoesNotExist:
            abort(404)
        except Exception as e:
            print(e)
            abort(500)

    form = create_ad_form(user if is_for_another_user else current_user)
    if form.validate_on_submit():
        wallet_address = form.wallet.data
        coin = form.coin.data
        max_amount = form.max_amount.data
        min_amount = form.min_amount.data
        if min_amount > max_amount:
            flash('min amount cant be greater than max amount.')
            return redirect(url_for('advertisement.create_ad'))
        if min_amount < 0 or max_amount < 0:
            flash('Kidding me ?! amounts cant be lower than zero.')
            return redirect(url_for('advertisement.create_ad'))
        if coin == 'ETH':
            try:
                balance = EthApi.get_wallet_info(wallet_address)['balance']
            except EthExplorerException as e:
                flash(e)
                return redirect(url_for('advertisement.create_ad'))
            if max_amount > balance:
                flash('max amount cant be greater than balance.')
                return redirect(url_for('advertisement.create_ad'))

        elif coin == 'BTC':
            balance = BitcoinApi.get_wallet_balance(wallet_address)
            if max_amount > balance:
                print('greater than balance.')
                flash('max amount cant be greater than balance.')
                return redirect(url_for('advertisement.create_ad'))

        if is_for_another_user:
            try:
                ad = Advertisement(id=uuid4().hex, dollar_price=form.dollar_price.data,
                                   min_amount=form.min_amount.data, max_amount=form.max_amount.data,
                                   coin=form.coin.data, date=datetime.utcnow(),
                                   per_unit_price=form.per_unit_price.data,
                                   price_model=form.price_model.data,
                                   wallet_type=form.wallet_type.data,
                                   wallet=form.wallet.data,
                                   commission_type=form.commission_type.data,
                                   status='ACTIVE',
                                   reserved_amount=0.00,
                                   private=form.private.data)
                User.append_ad(username, ad)
                flash(gettext('ad created successfully for user %s!') % username)
                return redirect(url_for('advertisement.ads_list', username=username))
            except MongoEngineErrors.DoesNotExist:
                abort(404)
            except Exception as e:
                print(e)
                abort(500)
        else:
            try:
                print('here2')
                ad = Advertisement(id=uuid4().hex, dollar_price=form.dollar_price.data,
                                   min_amount=form.min_amount.data, max_amount=form.max_amount.data,
                                   coin=form.coin.data, date=datetime.utcnow(),
                                   per_unit_price=form.per_unit_price.data,
                                   price_model=form.price_model.data,
                                   wallet_type=form.wallet_type.data,
                                   wallet=form.wallet.data,
                                   commission_type=form.commission_type.data,
                                   status='ACTIVE',
                                   reserved_amount=0.00,
                                   private=form.private.data)
                current_user.append_ad(current_user.username, ad)
                flash(gettext('ad created successfully!'))
                return redirect(url_for('advertisement.ads_list', username=username))
            except MongoEngineErrors.DoesNotExist:
                abort(404)
            except Exception as e:
                print(e)
                abort(500)
    else:
        return render_template('/create.html', form=form, user=current_user)


# TODO just can be deactivate if and only if advertisement doesn't have any active request
@advertisement_blueprint.route('/archive/<string:ad_id>/')
# @ad_owner_or_admin
@login_required
def archive_ad(ad_id):
    try:
        current_user.ad_archive(ad_id)
        flash('Ad archived')
    except MongoEngineErrors.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)
    return redirect(url_for('advertisement.ads_list'))


# @advertisement_blueprint.route('/update', methods=['get', 'post'])
# @advertisement_blueprint.route('/update/<string:ad_id>', methods=['get', 'post'])
# @login_required
# @ad_owner_or_admin
# def update_an_item(item_id=None):
#     item = Advertisement.get_by_id(item_id)
#     if request.method == 'POST':
#         form = Create()
#         Advertisement.update(id=item_id, coin=form.coin.data, amount=form.amount.data,
#                              price_model=form.price_model.data, dollar_price=form.dollar_price.data,
#                              per_unit_price=form.per_unit_price.data, higgle=form.higgle.data,
#                              private=form.private.data, wallet=form.wallet.data)
#         return redirect(url_for('item.list_items', username=item['owner']))
#     else:
#         print('this is test')
#         form = Create(coin=item['coin'], amount=item['amount'], price_model=item['price_model'],
#                       dollar_price=item['dollar_price'], per_unit_price=item['per_unit_price'], wallet=item['wallet'],
#                       higgle=item['higgle'], private=item['private'])
#         return render_template('update.html', item=item, form=form)


@advertisement_blueprint.route('/detail/<string:ad_id>/')
# @ad_owner_or_admin
@login_required
def detail(ad_id=None):
    advertisement = User.get_ad_for_all(ad_id)[0]
    return render_template('detail.html', ad=advertisement)


# @advertisement_blueprint.route('/list')
@advertisement_blueprint.route('/list/')
def list():
    #TODO add filter box in ui
    page = request.args.get('page') if request.args.get('page') != None else 1
    limit = request.args.get('limit') if request.args.get('limit') != None else 10
    coin = request.args.get('coin') if request.args.get('coin') != None else False
    min_amount_gt = request.args.get('min_amount_gt') if request.args.get('min_amount_gt') != None else False
    min_amount_lt = request.args.get('min_amount_lt') if request.args.get('min_amount_lt') != None else False
    max_amount_gt = request.args.get('max_amount_gt') if request.args.get('max_amount_gt') != None else False
    max_amount_lt = request.args.get('max_amount_lt') if request.args.get('max_amount_lt') != None else False
    commission_type = request.args.get('commission_type') if request.args.get('commission_type') != None else False
    filters = {'page': page, 'limit': limit, 'coin': coin, 'min_amount_gt': min_amount_gt,
               'min_amount_lt': min_amount_lt, 'max_amount_gt': max_amount_gt, 'max_amount_lt': max_amount_lt,
               'commission_type': commission_type}
    query = current_user.query_builder(filters)
    results = User.objects.aggregate(*query)
    results = [i for i in results]
    return render_template('advertisement/list.html', results=results)
