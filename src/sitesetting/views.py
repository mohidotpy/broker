from flask import render_template, flash, redirect, url_for
from . import sitesetting_blueprint
from src.misc.requires_roles import requires_roles
from flask_login import login_required
from .forms import SiteSettingForm
from .models import SiteSetting


@login_required
@requires_roles('ADMIN')
@sitesetting_blueprint.route('/', methods=['get','post'])
def site_setting():
    form = SiteSettingForm()
    if form.validate_on_submit():
        SiteSetting.objects.get(name='DEFAULT').update(commision_rate=float(form.commision_rate.data), upsert=True)
        flash('global commission rate updated!')
        return redirect(url_for('sitesetting.site_setting'))
    else:
        return render_template('site_setting.html', form=form)