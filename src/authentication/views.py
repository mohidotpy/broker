from flask import render_template, request, redirect, url_for, flash, session, abort, Markup
from flask_login import login_required, login_user, logout_user, current_user
from mongoengine import errors as MongoEngineExceptions
from src import login_manager
from src.authentication import authentication_blueprint
from .forms import LoginForm, RegistrationForm, EmailForm, PhonenumberForm
from .models import AuthenticationManager
from src.misc.helpers import is_safe_url, get_verification_link, get_decoded_code_and_username, get_password_reset_link
from src.user.models import User
# from src import email_worker
from src.misc.exceptions import EmailNotVerified
from src.misc.constants import USER_INFO_STATUS,SEEN_STATUS
import re
from uuid import uuid4
from src.notification.models import Notification


@authentication_blueprint.route('/login/', methods=['GET', 'POST'])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('home.index'))
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = AuthenticationManager.authenticate_user(form.phonenumber_or_email.data, form.password.data)
            if user:
                login_user(user, remember=form.remember_me.data)
                session['user_id'] = user.username
                user.update(reset_password_status=USER_INFO_STATUS['NORMAL'])
                flash('Logged in successfully.')
                # processing next redirection
                next_url = request.args.get('next')
                if not is_safe_url(next_url):
                    return abort(400)
                else:
                    '''process next page prevent from access to redirect after login'''
                    next_args = request.args.to_dict()
                    try:
                        next_endpoint = next_args['next'] if next_args['next'] else False
                        if next_endpoint:
                            del next_args['next']
                            return redirect(url_for(next_endpoint, **next_args))
                        else:
                            return redirect(url_for('home.index'))
                    except:
                        return redirect(url_for('home.index'))

            flash('Incorrect password.')
            return redirect(url_for('authentication.login'))
        except MongoEngineExceptions.DoesNotExist:
            flash('User does not exist.')
            return redirect(url_for('authentication.login'))
        except EmailNotVerified as data:
            flash('email has not verified yet. you can either go back to'
                  ' login page and login with username or phone number or'
                  'proceed to verify your email address using form below.')
            return redirect(url_for('authentication.resend_verification_email', username=data.username))
    return render_template('login.html', title='Login', form=form, args=request.args)


@authentication_blueprint.route('/resend_verification_email/<string:username>/', methods=['GET', 'POST'])
def resend_verification_email(username):
    form = EmailForm()
    if form.validate_on_submit():
        user = User.get(username)
        email = form.email.data
        if email == user.email and user.email_status == USER_INFO_STATUS['PENDING']:
            link = get_verification_link(user.email_verification_code, username)
            # email_worker.enqueue_email(email, 'Verification for Securypto', link, html=link)
            flash('successfully sent email verification to %s' % email)
            return redirect(url_for('authentication.login'))
        else:
            flash('email %s is not valid. sorry!' % email)
    try:
        user = User.get(username)
        email = user.email
        if user.email_status == USER_INFO_STATUS['ACCEPTED']:
            flash('email is already verified.')
            return redirect(url_for('authentication.login'))
        if not current_user.is_authenticated:
            idx = email.index('@')
            if email.index('@') <= 3:
                for index, ch in enumerate(email):
                    if index < idx:
                        email = email[:index] + '*' + email[index + 1:]
            else:
                email_p1 = email[:3]
                email_p2 = email[idx:]
                email = email_p1 + '*' * (idx - 3) + email_p2
    except MongoEngineExceptions.DoesNotExist:
        return abort(404)
    return render_template('resend_verification_email.html', form=form, user=user, email_secret=email)


# TODO add exception handling for users with session that we have removed from the database
@login_manager.user_loader
def load_user(phonenumber_or_email_or_username):
    user = User.get(phonenumber_or_email_or_username)

    try:
        unseen_notif = Notification.get_unseen_notification(user.username)
        user.unseen_notifications = unseen_notif['unseen_notif']
        notif_count = Notification.notification_count(user.username)
        user.notification_count = notif_count

    except MongoEngineExceptions.DoesNotExist as e:
        user.notification_count = 0
    return user


@login_manager.unauthorized_handler
def handle_needs_login():
    flash("You have to be logged in to access this page.")
    params = request.view_args if request.view_args != None else {}
    args = request.args
    return redirect(url_for('authentication.login', next=request.endpoint, *params, **args))


@authentication_blueprint.route('/register/', methods=['GET', 'POST'])
def register():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('home.index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        try:
            if not User.objects(username=form.username.data):
                # insert notification collection in db
                notifications = Notification.send_notification(username=form.username.data)

                # signup user
                AuthenticationManager.register_user(form.username.data, form.phonenumber.data, form.password.data,
                                                    notifications, form.email.data)

                flash('email verification sent. please check your email for verifying.')
                return redirect(url_for('home.index'))
            else:
                flash('User with username "{}" already exists. Please choose a different username.'.format(form.username.data))
                return redirect(url_for('authentication.register'))
        except MongoEngineExceptions.NotUniqueError as error:
            s = re.search('index\:\ (?:.*\.)?\$?(?:([_a-z0-9]*)(?:_\d*)|([_a-z0-9]*))\s*dup key: { : "(.*)"',
                          str(error),
                          re.M | re.I)
            field = s.group(1)
            value = s.group(3)
            print(s.group())
            flash(Markup('User already registered, user with "{}" of "{}" already exists. If you forgot your password please click <a href="{}">here</a>'.format(field, value, url_for('authentication.forgot_password', _external=True))))
            return redirect(url_for('authentication.register'))
        except MongoEngineExceptions.ValidationError as error:
            print(error)
            flash('Some fields cant be validated.')
            return redirect(url_for('authentication.register'))
        except Exception as error:
            print(error)
            return abort(500)
    return render_template('register.html', title='Register', form=form)


@authentication_blueprint.route('/verify_email/<string:code>/')
def verify_email(code):
    code, username = get_decoded_code_and_username(code)
    try:
        user = User.get(username)
        if user.email_verification_code == code:
            user.verify_email()
            login_user(user, remember=False)
            session['user_id'] = user.username
            flash('email verified successfully.')
            return redirect(url_for('home.index'))
        else:
            flash('verification code is wrong. please go to profile page to resend verification code.')
            return redirect(url_for('home.index'))
    except MongoEngineExceptions.DoesNotExist:
        flash('User does not exists or verfication code is incorrect.')
        return redirect(url_for('authentication.login'))
    except Exception as error:
        print(error)
        flash('Database error.')
        return redirect(url_for('authentication.login'))


@authentication_blueprint.route('/verify_password_reset/<string:code>/')
def verify_password_reset(code):
    code, username = get_decoded_code_and_username(code)
    try:
        user = User.get(username)
        if user.email_verification_code == code:
            user.update(reset_password_status=USER_INFO_STATUS['CHANGE_PASSWORD'])
            login_user(user, remember=False)
            session['user_id'] = user.username
            return redirect(url_for('user.change_password', reset_code=code))
        else:
            flash('verification code is wrong. please go to profile page to resend verification code.')
            return redirect(url_for('home.index'))
    except MongoEngineExceptions.DoesNotExist:
        flash('User does not exists or verfication code is incorrect.')
        return redirect(url_for('authentication.login'))
    except Exception as error:
        print(error)
        flash('Database error.')
        return redirect(url_for('authentication.login'))


@authentication_blueprint.route('/forgot-password/')
def forgot_password():
    return render_template('forgot_password.html')


@authentication_blueprint.route('/reset-password/', methods=['GET', 'POST'])
def reset_password():
    method = request.args.get('method')
    if method == 'EMAIL':
        form = EmailForm()
    elif method == 'PHONENUMBER':
        form = PhonenumberForm()
    else:
        abort(404)
    if form.validate_on_submit():
        email = form.data.get('email')
        phonenumber = form.data.get('phonenumber')
        try:
            if email:
                user = User.objects.get(email=email)
                if user.email_status == USER_INFO_STATUS['ACCEPTED']:
                    user.update(email_verification_code=uuid4().hex)
                    link = get_password_reset_link(user.email_verification_code, user.username)
                    from src.misc.email_notification_worker import send_async_email
                    send_async_email.delay(form.email.data, 'Password Reset for Securypto', link, html=link)
                    flash('password reset email sent successfully.')
                    return redirect(url_for('home.index'))
                else:
                    flash('email is not verified. you cant reset password with unverified email address.')
                    return redirect(url_for('authentication.login'))
            elif phonenumber:
                user = User.objects.get(phonenumber=phonenumber)
            else:
                abort(404)
        except MongoEngineExceptions.DoesNotExist:
            flash('user with sepecified email not found.')
            return redirect(url_for('authentication.login'))
        except Exception as error:
            print(error)
            abort(500)
    return render_template('reset_password.html', method=method, form=form)


@authentication_blueprint.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect(url_for('authentication.login'))
