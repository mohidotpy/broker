from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired, Length, Regexp, EqualTo, Email, Optional
from flask_wtf.recaptcha import RecaptchaField

class LoginForm(FlaskForm):
    phonenumber_or_email = StringField(
        'Phone Number or Email or Username', id='phonenumber_or_email', validators=[InputRequired()])
    password = PasswordField('Password', id='password', validators=[
        InputRequired()])
    remember_me = BooleanField('Remember Me', id='remember_me')
    recaptcha = RecaptchaField()


class RegistrationForm(FlaskForm):
    username = StringField('Username', id='username', validators=[InputRequired(), Regexp('^[a-zA-Z0-9_]{3,50}$', message='Only alphanumeric and underscore')])
    phonenumber = StringField('Phone Number', id='phonenumber', validators=[InputRequired(), Regexp('^(\+989|09)\d{9,}$', message='Phone number must be in one of following formats: +989xxxxxxxxx, 09xxxxxxxxx')])
    email = EmailField('Email (Optional)', id='email', validators=[Email(), Optional()])
    password = PasswordField('Password', id='password', validators=[InputRequired(), Length(min=8, max=256),
                                                                    EqualTo('repeat_password',
                                                                            message='Passwords doesnt match.'),
                                                                    Regexp('^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,128}$',
                                                                           message='At least 8 characters, one letter and one number. Maximum length 128 characters.')])
    repeat_password = PasswordField(
        'Repeat Password', id='repeat_password', validators=[InputRequired()])
    accpet_terms = BooleanField(
        'I accept terms and conditions.', validators=[InputRequired()])
    recaptcha = RecaptchaField()

class EmailForm(FlaskForm):
    email = StringField('Email', id='email', validators=[InputRequired(), Email()])
    recaptcha = RecaptchaField()

class PhonenumberForm(FlaskForm):
    phonenumber = StringField('Phonenumber', id='phonenumber', validators=[InputRequired()])
    recaptcha = RecaptchaField()