from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, SelectField, PasswordField, FloatField
from wtforms.validators import Length, InputRequired, Email, EqualTo
from src.coin.models import Coin


class AddWalletForm(FlaskForm):
    COIN_LIST = Coin.get_from_db()
    coin = SelectField(label='Wallet For', id='coin',
                       choices=[(i['symbol'], i['symbol'] + ' - ' + i['name'].title()) for i in COIN_LIST])
    address = StringField(label='Wallet Address', id='address', validators=[Length(max=512), InputRequired()])
    name = StringField(label='Name', id='name', validators=[Length(min=2, max=128), InputRequired()])


class EmailForm(FlaskForm):
    email = StringField('Email', id='email', validators=[InputRequired(), Email()])


class ChangePasswordForm(FlaskForm):
    current_password = PasswordField('Current Password', id='current_password',
                                     validators=[InputRequired(), Length(min=8, max=128)])
    password = PasswordField('New Password', id='password', validators=[InputRequired(), Length(min=8, max=128),
                                                                        EqualTo('repeat_password',
                                                                                message='Passwords doesnt match.')])
    repeat_password = PasswordField(
        'Repeat New Password', id='repeat_password', validators=[InputRequired()])


class ResetPasswordForm(FlaskForm):
    password = PasswordField('New Password', id='password', validators=[InputRequired(), Length(min=8, max=128),
                                                                        EqualTo('repeat_password',
                                                                                message='Passwords doesnt match.')])
    repeat_password = PasswordField(
        'Repeat New Password', id='repeat_password', validators=[InputRequired()])


class ProfilePhotoForm(FlaskForm):
    profile_photo = FileField('Profile Photo:',
                              id='profile_photo', validators=[FileRequired()])

class ChangeCommissionDiscount(FlaskForm):
    commission = FloatField(label='commission_rate', id='commission_rate',
                           validators=[InputRequired()])
