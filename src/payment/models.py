from src.misc.bp_codes import BpCodes
from datetime import datetime
from src import payment_client, db
from src.misc.exceptions import BpException
from src.misc.constants import PAYMENT_DIRECTION, PAYMENT_TYPE, PAYMENT_STATUS
from src import app, Serializer
from src.misc.helpers import get_numeric_id
from mongoengine import errors as MongoEngineExceptions
from mongoengine.queryset.visitor import Q
import json
class GatewayData(db.EmbeddedDocument):
    ref_id = db.StringField()
    pay_res_code = db.IntField()
    sale_order_id = db.IntField()
    sale_reference_id = db.IntField()
    verify_res_code = db.IntField()
    inquiry_res_code = db.IntField()
    settle_res_code = db.IntField()
    card_holder_pan = db.StringField()
    card_holder_info = db.StringField()


class Payment(db.Document):
    order_id = db.IntField(unique=True)
    direction = db.StringField(choices=PAYMENT_DIRECTION)
    type = db.StringField(choices=PAYMENT_TYPE)
    user = db.StringField()
    request = db.ReferenceField('Request')
    total_price = db.DecimalField(min_value=0, precision=8)
    commission_price = db.DecimalField(min_value=0, precision=8)
    status = db.StringField(choices=PAYMENT_STATUS, default=PAYMENT_STATUS['NOT_PAID'])
    gateway_data = db.EmbeddedDocumentField(GatewayData)
    checked_out = db.BooleanField(default=False)
    needs_refund = db.BooleanField(default=False)
    can_withdraw = db.BooleanField(default=False)

    meta = {
        'indexes': ['order_id']
    }

    def __repr__(self):
        return '[Payment] Id: %s, Order Id: %s, Status: %s, user: %s' % (
            self.id, self.order_id, self.status, self.user)

    @staticmethod
    def get_payment_ref_id(payment_id, amount, order_id, payer_id=0):
        date_now = str(datetime.today()).split(' ')[0].replace('-', '')
        time_now = str(datetime.today()).split(' ')[1].split('.')[0].replace(':', '')
        # TODO: Check DynamicPayRequest Mohammad Mosazadeh
        data = json.dumps({'payment_id': str(payment_id)})
        result = payment_client.service.bpPayRequest(terminalId=app.config['BP_TERMINAL_ID'],
                                                     userName=app.config['BP_USERNAME'],
                                                     userPassword=app.config['BP_PASSWORD'],
                                                     orderId=order_id, amount=amount, localDate=date_now,
                                                     localTime=time_now,
                                                     additionalData=data,
                                                     callBackUrl=app.config['BP_CALLBACK_URL'], payerId=payer_id)
        # TODO add bank test
        print('result is: ')
        print(str(result))
        ref_id = None
        code = int(result.split(',')[0])
        if code == BpCodes.SUCCESSFUL.value:
            ref_id = result.split(',')[1]
            print(ref_id)
            return ref_id
        raise BpException(code)

    @staticmethod
    def verify_payment(order_id, sale_order_id, sale_reference_id):
        result = payment_client.service.bpVerifyRequest(terminalId=app.config['BP_TERMINAL_ID'],
                                                        userName=app.config['BP_USERNAME'],
                                                        userPassword=app.config['BP_PASSWORD'],
                                                        orderId=order_id, saleOrderId=sale_order_id,
                                                        saleReferenceId=sale_reference_id)
        code = int(result)
        if code == BpCodes.SUCCESSFUL.value:
            return code
        else:
            raise BpException(code)

    @staticmethod
    def inquiry_payment(order_id, sale_order_id, sale_reference_id):
        result = payment_client.service.bpInquiryRequest(terminalId=app.config['BP_TERMINAL_ID'],
                                                         userName=app.config['BP_USERNAME'],
                                                         userPassword=app.config['BP_PASSWORD'],
                                                         orderId=order_id, saleOrderId=sale_order_id,
                                                         saleReferenceId=sale_reference_id)
        code = int(result)
        if code == BpCodes.SUCCESSFUL.value:
            return code
        else:
            raise BpException(code)

    @staticmethod
    def settle_payment(order_id, sale_order_id, sale_reference_id):
        result = payment_client.service.bpSettleRequest(terminalId=app.config['BP_TERMINAL_ID'],
                                                        userName=app.config['BP_USERNAME'],
                                                        userPassword=app.config['BP_PASSWORD'],
                                                        orderId=order_id, saleOrderId=sale_order_id,
                                                        saleReferenceId=sale_reference_id)
        code = int(result)
        if code == BpCodes.SUCCESSFUL.value or code == BpCodes.SETTLED_TRANSACTION.value:
            return code
        else:
            raise BpException(code)

    @classmethod
    def get(cls, id):
        return cls.objects.get(id=id)

    @classmethod
    def get_by_order_id(cls, order_id):
        return cls.objects.get(order_id=order_id)

    @classmethod
    def is_owner(cls, username, pay_id):
        pay = cls.objects.get(id=pay_id)
        return True if pay.user == username else False

    @classmethod
    def get_by_filter(cls, username, **filters):
        return cls.objects(user=username, **filters)

    # TODO upload to a folder and submit for admin review
    @classmethod
    def paya_export(cls):
        pass

    @classmethod
    def get_unique_id(cls):
        while True:
            id = get_numeric_id()
            try:
                cls.get_by_order_id(id)
            except MongoEngineExceptions.DoesNotExist:
                return id

    @classmethod
    def get_unsettled_payments(cls):
        return cls.objects(Q(status=PAYMENT_STATUS['PAID']) & (Q(gateway_data__verify_res_code=BpCodes.SUCCESSFUL.value) | Q(gateway_data__inquiry_res_code=BpCodes.SUCCESSFUL.value) | Q(gateway_data__inquiry_res_code=BpCodes.SETTLED_TRANSACTION.value)))

    @classmethod
    def get_withdrawn_payments(cls, username):
        return cls.objects(type=PAYMENT_TYPE['WITHDRAWAL'], checked_out=True, user=username)

    @classmethod
    def get_unwithdrawn_payments(cls, username):
        return cls.objects(type=PAYMENT_TYPE['WITHDRAWAL'], checked_out=False, user=username)

    @classmethod
    def get_all_withdrawals(cls, username):
        return cls.objects(type=PAYMENT_TYPE['WITHDRAWAL'], user=username)