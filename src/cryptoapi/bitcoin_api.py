from src import cache
from moneywagon import get_address_balance, get_historical_transactions, get_single_transaction, AddressBalance
from moneywagon.services import BlockChainInfo, BlockCypher, BlockSeer, SmartBitAU, Winkdex, ChainSo
import requests


class BitcoinApi:
    @staticmethod
    def get_wallet_transactions(address):
        result = get_historical_transactions('btc', address)
        return result

    @staticmethod
    def get_transaction_info(tx_hash):
        result = get_single_transaction('btc', tx_hash, [BlockChainInfo, BlockCypher, ChainSo, BlockSeer, SmartBitAU, Winkdex])
        return result

    @staticmethod
    def get_wallet_balance(address):
        result = get_address_balance('btc', address)
        return result

    @staticmethod
    @cache.cached(timeout=3600, key_prefix='bitcoin_fee')
    def get_current_fee():
        result = requests.get('https://bitcoinfees.earn.com/api/v1/fees/recommended').json()
        half_hour_fee = result.get('halfHourFee')
        return half_hour_fee
    #TODO: Deduct 1 tx fee from ad max amount. for every request reserved amount should be amount + tx_fee. update reserved amounts every hour.