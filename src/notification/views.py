from . import notification_blueprint
from flask import render_template
from flask_login import login_required, current_user
from .models import Notification


@notification_blueprint.route('/')
@login_required
def all():
    for nt in current_user.notifications:
        print(nt)
    return render_template('all-notification.html')
