from src import db
from src.misc.exception_log import ExceptionLogTypes
from src.misc.constants import MODEL_NAMES


class DatabaseObject(db.EmbeddedDocument):
    model_name = db.StringField(choices=MODEL_NAMES)
    id = db.StringField()


class ExceptionLog(db.Document):
    type = db.IntField(choices=ExceptionLogTypes)
    stack_trace = db.StringField()
    file = db.StringField()
    route = db.StringField()
    database_objects = db.ListField(db.EmbeddedDocumentField(DatabaseObject))
    user = db.StringField()
    is_resolved = db.BooleanField(default=False)
    is_critical = db.BooleanField(default=False)

    @classmethod
    def mark_as_critical(cls, id):
        return cls.objects(id=id).update(is_critical=True)

    @classmethod
    def get_unresolved_exceptions(cls):
        return cls.objects(is_resolved=False)

    @classmethod
    def get_resolved_exceptions(cls):
        return cls.objects(is_resolved=True)

    @classmethod
    def mark_as_resolved(cls, id):
        return cls.objects(id=id).update(is_resolved=True)

