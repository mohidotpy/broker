from urllib.parse import urlparse, urljoin
from flask import request,abort
import config
from .constants import CHAT_TYPES
import arrow
from khayyam3 import JalaliDatetime
import os
from base64 import b64encode, b64decode
import datetime
import json
import random
from uuid import uuid4


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc

def get_type_from_extension(ext):
    ext = ext[1:]
    print(ext)
    if ext in config['IMAGE_ALLOWED_EXTENSIONS']:
        return CHAT_TYPES['IMAGE']

    if ext in config['VIDEO_ALLOWED_EXTENSIONS']:
        return CHAT_TYPES['VIDEO']

    if ext in config['AUDIO_ALLOWED_EXTENSIONS']:
        return CHAT_TYPES['AUDIO']

    if ext in config['DOCUMENT_ALLOWED_EXTENSIONS']:
        return CHAT_TYPES['DOCUMENT']

    raise TypeError('Type {} is not allowed in server, but found in one of user files.'.format(ext))

def get_iran_datetime(datetime, format='%C'):
    timestamp_iran = arrow.get(datetime).to('Asia/Tehran').timestamp
    return JalaliDatetime.fromtimestamp(timestamp_iran).strftime(format)

def get_email_template(name):
    return open(os.path.join(config.BASE_DIR, 'src', 'static', 'email_templates', (name + '.html'))).read()

def get_verification_link(code, username):
    code_and_username = code + ':' + username
    code_and_username_encoded = b64encode(code_and_username.encode('ascii')).decode('ascii')
    return os.path.join(config.SITE_BASE_URL, 'authentication/verify_email', code_and_username_encoded)

def get_password_reset_link(code, username):
    code_and_username = code + ':' + username
    code_and_username_encoded = b64encode(code_and_username.encode('ascii')).decode('ascii')
    return os.path.join(config.SITE_BASE_URL, 'authentication/verify_password_reset', code_and_username_encoded)

def get_decoded_code_and_username(code):
    code_and_username = b64decode(code.encode('ascii')).decode('ascii')
    decoded_code = code_and_username.split(':')[0]
    username = code_and_username.split(':')[1]
    return decoded_code, username

def check_transaction_time(time, timedelta):
    return time + datetime.timedelta(0, timedelta) >= datetime.datetime.utcnow()

def compare_times(t1, t2, seconds):
    return t1 + datetime.timedelta(seconds) >= t2


def serialize_json(**kwargs):
    return json.dumps(kwargs)

def get_rand_int(n):
    return ''.join(["%s" % random.randint(0, 9) for i in range(0, n)])

def get_numeric_id():
    return str(uuid4().int)[:9]