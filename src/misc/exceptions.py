class Banned(Exception):
    pass


class IncorrectPassword(Exception):
    pass


class NotUnique(Exception):
    """
    Raised when user input is not unique.
    """
    pass


class InvalidClientData(Exception):
    """
    Raised when server data has changed in between user sessions.
    """
    pass


class NotFound(Exception):
    pass


class EmailNotVerified(Exception):
    def __init__(self, username):
        self.username = username


class BadStatus(Exception):
    pass


class EthExplorerException(Exception):
    pass


class NotEnoughConfirmations(Exception):
    pass


class BpException(Exception):
    pass


class UnsupportedCoin(Exception):
    pass


class NotVotedYet(Exception):
    pass


class NoConflict(Exception):
    pass
